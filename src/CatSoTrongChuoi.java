import java.util.Scanner;

public class CatSoTrongChuoi {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.next();
//        for(int i = 0;i< str.length();i++)
//        {
//            char c = str.charAt(i);
//            if (c >= '0' && c <= '9')
//                str = str.replace(c + "","");
//        }
//        System.out.println(str);

        // \d nghĩa là một chữ số (Ký tự trong phạm vi 0-9) và +có nghĩa là 1 hoặc nhiều lần. Vì vậy, \d+là 1 hoặc nhiều chữ số.
        str = str.replaceAll("\\d+","");
        System.out.print(str);
    }
}
