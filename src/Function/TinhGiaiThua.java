package Function;

import java.util.Scanner;

public class TinhGiaiThua {

    public static int tinhGiaiThua(int a)
    {
        int giaiThua=1;
        for(int i = 1;i<=a;i++)
        {
            giaiThua*=i;
        }
        return giaiThua;
    }

    public  static int nhapSoNguyen()
    {
        Scanner scan = new Scanner(System.in);
        int n;
        do{
            System.out.print("Nhập n = ");
            n=scan.nextInt();
        }while (n<0);
        return n;
    }

    public static void main(String[] args) {
        int n = nhapSoNguyen();
        System.out.println(n+"! = "+tinhGiaiThua(n));
    }
}
