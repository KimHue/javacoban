package Function;

import java.util.Scanner;

public class inCHuoiInHoa {

    public static String nhapChuoi()
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhap chuoi: ");
        String str = scan.nextLine();
        return str;
    }

    public  static void inKiTuINHOA(String str){
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)>='A'&&str.charAt(i)<='Z')
            {
                System.out.print(str.charAt(i));
            }
        }
    }

    public static void main(String[] args) {
        String str= nhapChuoi();
        inKiTuINHOA(str);
    }
}
