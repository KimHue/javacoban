package Function;

import java.util.Scanner;

public class BCNN {
    public static int nhapSoNguyenDuong()
    {
        Scanner scan = new Scanner(System.in);
        int number;
        do{
            System.out.print("Nhập so nguyen: ");
            number = scan.nextInt();
        }while (number<0);
        return number;
    }

    public  static int timUCNN(int a, int b){
        int ucln;
        for(ucln=Math.min(a,b);ucln>=1;ucln--)
        {
            if(a % ucln == 0 && b % ucln == 0)
                break;
        }
        return ucln;
    }

    public static int timBSCNN(int a, int b) {
        return (a*b)/timUCNN(a,b);
    }

    public static void main(String[] args) {
        int a = nhapSoNguyenDuong();
        int b = nhapSoNguyenDuong();
        System.out.println("BCNN = "+timBSCNN(a,b));
    }
}
