package Function;

import java.util.Scanner;

public class BT1_NgayThangNam {
    // Hàm Menu
    public static void printMenu() {
        System.out.println("-----------------------MENU---------------------------"
                + "\n1.In ra tổng số ngày trong tháng." +
                "\n2.In ra năm đó có phải năm nhuận hay không." +
                "\n3.In ra ngày kế tiếp." +
                "\n4.In ra ngày trước đó." +
                "\n5.Thoát chương trình." +
                "\n------------------------------------------------------------");
    }

    // Ham chon Menu
    public static int chooseMenu() {
        Scanner scanner = new Scanner(System.in);
        int chon;
        do {
            chon = inputData();
            if (chon > 6 || chon < 1) {
                System.out.println("không hợp lệ, hãy nhập lựa chọn từ 1 - 5!");
            }
        } while (chon > 6 || chon < 1);
        return chon;
    }

    //Hàm kiểm tra năm nhuận
    public static boolean checkLeapYear(int year) {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)// la nam nhuan
            return true;
        else
            return false;
    }

    //Hàm nhập ngay,thang,nam
    public static int inputData() {
        Scanner scan = new Scanner(System.in);
        int n;
        n = scan.nextInt();
        return n;
    }

    // hàm kiểm tra nam
    public static boolean checkYear(int year) {
        boolean flag = year > 0 ? true : false;
        return flag;

    }

    // hàm kiểm tra tháng
    public static boolean checkMonth(int month) {
        boolean flag = (month < 1 || month > 12) ? false : true;
        return flag;

    }

    // hàm Kiem tra ngày trong tháng
    public static int DayinMonth(int month, int year) {
        int top = 0;
        switch (month) {
            case 4:
            case 6:
            case 9:
            case 11:
                top = 30;
                break;
            case 2:
                if (checkLeapYear(year))
                    top = 29;
                else top = 28;
                break;
            default:
                top = 30;
        }
        return top;
    }

    // Ham kiểm tra ngày
    public static boolean checkDay(int day, int month, int year) {
        if (day < 1 || day > DayinMonth(month, year))
            return false;
        else
            return true;
    }

    //In ra ngay ke tiep
    public static void printTheNextDay(int day, int month, int year) {
        if (day == DayinMonth(month, year)) {
            if (month == 12) {
                day = 1;
                month = 1;
                year++;
            } else {
                day = 1;
                month++;
            }

        } else
            day++;
        System.out.println("Ngay tiep theo la: " + day + "/" + month + "/" + year);
    }

    // Hamf tinh ngay hom truoc
    public static void printTheBeforeDay(int day, int month, int year) {
        day--;
        if (day == 0) {
            month--;
            if (month == 0) {
                month = 12;
                year--;
            }
            day = DayinMonth(month, year);
        }
    }

    public static void checkDate(int day, int month, int year) {


        if (!checkDay(day, month, year)) {
            System.out.println("Không có ngày này trong tháng!!!");

        }
        if (!checkMonth(month)) {
            System.out.println("Không có tháng này trong năm!!!");

        }
        if (!checkYear(year)) {
            System.out.println("Không tồn tại năm này!!!");

        }
    }
    static int day, month, year;

    // Nhap va check du lieu dau vao
    public static void Input() {
        //int day, int month, int year
        Scanner scan = new Scanner(System.in);

        System.out.println("Nhap ngay : ");
        day = inputData();
        System.out.println("Nhap thang: ");
        month = inputData();
        System.out.println("Nhap nam: ");
        year = inputData();
        checkDate(day, month, year);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        printMenu();
        int chon = chooseMenu();

        switch (chon){
            case 1:
                Input();
                System.out.println("Tổng số ngày trong tháng " + month + " năm " + year + " là: " + DayinMonth(month, year));
                break;
            case 2:
                Input();
                if(checkLeapYear(year))
                    System.out.println("Là năm nhuận !");
                else
                    System.out.println("k phải là năm nhuận");
                break;
            case 3:
                Input();
                printTheNextDay(day,month,year);
                break;
            case 4:
                Input();
                printTheBeforeDay(day,month,year);
                break;
            case 5:
                break;
        }
    }
}
