package Function;

import java.util.Scanner;

public class BT16_DemSoTu {
    public static String str;

    public static void nhapChuoi() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Hãy nhập chuỗi bất kì: ");
        str = scan.nextLine();
    }

    public static int demTuTrongChuoi(String str) {
        str = str.trim();
        int dem = 1;
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) == ' ' && str.charAt(i - 1) != ' ')
                dem++;
        }
        return dem;
    }

    public static void main(String[] args) {
        nhapChuoi();
        System.out.println(str);
        System.out.println("Có " + demTuTrongChuoi(str) + " từ trong chuỗi!");
    }

}
