package Function;

import java.util.Locale;
import java.util.Scanner;

public class BT20_DemKiTuNguyenAmVaPhuAm {
    public static String str;

    public static void nhapChuoi() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Hãy nhập chuỗi bất kì: ");
        str = scan.nextLine();

    }

    public static void demNguyenAmVaPhuAm(String str) {
        str = str.toLowerCase();
        int dem1 = 0, dem2 = 0;
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                dem1++;
            } else if ((ch >= 'a' && ch <= 'z')) {
                dem2++;
            }
        }
        System.out.println(dem1 + " nguyên âm " + dem2 + " phụ âm");
    }


    public static void main(String[] args) {
        nhapChuoi();
        demNguyenAmVaPhuAm(str);
    }
}
