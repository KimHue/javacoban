package Function;

import java.util.Scanner;

public class DemKiTuTrongChuoi {
    public static String nhapChuoi()
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhap chuoi: ");
        String str = scan.nextLine();
        return str;
    }

    public  static char nhapKiTu()
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhap ki tu: ");
        char c = scan.next().charAt(0);
        return c;
    }

    public  static int demKiTu(char c, String s){
        int count =0;
        for(int i = 0;i<s.length();i++)
        {
            if(c==s.charAt(i))
                count++;
        }
        return count;
    }


    public static void main(String[] args) {
        String str = nhapChuoi();
        char c = nhapKiTu();
        System.out.println(demKiTu(c,str));
    }
}
