package Function;

import java.util.Scanner;

public class SoDoiXung {
    public  static int soDoiXung(int number) {
        int n = number, reversed = 0;
        while (number != 0) {
            int temp = number % 10;
            reversed = reversed * 10 + temp;
            number /= 10;
        }
       return reversed;
    }

    public static boolean laSoDoiXung(int number){
        if(soDoiXung(number)==number)
            return true;
        else return false;
    }

    public static int nhapSoNguyenDuong()
    {
        Scanner scan = new Scanner(System.in);
        int number;
        do{
            System.out.print("Nhập number = ");
            number = scan.nextInt();
        }while (number<0);
        return number;
    }

    public static void main(String[] args) {
        int number = nhapSoNguyenDuong();
        if(laSoDoiXung(number))
            System.out.println("La so doi xung");
        else
            System.out.println("K phai so doi xung");

    }
}
