package Function;

import java.util.Scanner;

public class BT17_DemSoKiTu {
    public static String nhapChuoi() {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        return str;
    }

    public static int demKiTuChu(String str) {
        int demKTChu = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i))) {
                demKTChu++;
            }

        }
        return demKTChu;
    }

    public static int demSo(String str) {
        int demSo = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                demSo++;
            }
        }
        return demSo;
    }

    public static int demKhoangTrang(String str) {
        int demKT = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                demKT++;
            }
        }
        return demKT;
    }

    public static  int demKTDB(String str){
        int demKTDB = 0;
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isLetterOrDigit(str.charAt(i))&&!Character.isWhitespace(str.charAt(i))) {
                demKTDB++;
            }
        }
        return demKTDB;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhập chuỗi: ");
        String str = nhapChuoi();
        System.out.println("Chuỗi vừa nhập có " + demSo(str) + " số, " + demKiTuChu(str) + " kí tự chữ "
                            + demKhoangTrang(str) + " khoảng trắng "+
                            demKTDB(str)+" kí tự đặc biệt!");
    }
}

