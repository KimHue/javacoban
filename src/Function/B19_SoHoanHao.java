package Function;

import java.util.Scanner;

public class B19_SoHoanHao {
    static int input;

    public static int nhapDL() {
        do {
            Scanner scanner = new Scanner(System.in);
            System.out.print("nhap so nguyen n = ");
            input = Integer.parseInt(scanner.nextLine());
            if (input < 0)
                System.out.println("Sai rồi! Hãy nhập số nguyên dương!");
        } while (input < 0);
        return input;
    }

    //Hàm tìm ước và tổng các ước
    public static int tinhTongUoc(int n) {
        int sum = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0)
                sum += i;
        }
        return sum;
    }

    // hàm là số hoàn hảo
    public static boolean laSoHH(int n) {
        boolean ok = (tinhTongUoc(n) == n) ? true : false;
        return ok;
    }


    public static void main(String[] args) {
        nhapDL();
        if(laSoHH(input))
            System.out.println(input+" là số hoàn hảo");
        else
            System.out.println(input+" k phải là số hoàn hảo");
    }
}