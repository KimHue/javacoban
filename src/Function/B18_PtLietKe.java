package Function;

import java.util.Scanner;

public class B18_PtLietKe {
    //la so nguyen to
    public static boolean isPrime(int n) {
        if (n < 2)
            return false;
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    //la so thuan nghich
    public static boolean isReversibleNumber(int n) {
        int temp = n, rever = 0;
        while (n != 0) {
            int resul = n % 10;
            rever = rever * 10 + resul;
            n /= 10;
        }
        if (rever == temp)
            return true;
        return false;
    }

    //Tong cac chu so
    public static int Sum(int n) {
        int sum = 0;
        while (n > 0) {
            int temp = n % 10;
            sum += temp;
        }
        return sum;
    }


    // Ham hien thi
    public static void print() {
        for (int i = 10000; i <= 9999999; i++) {
            if (isPrime(i) && isReversibleNumber(i)&&isReversibleNumber(Sum(i))) {
                System.out.print(i+" ");
            }
        }
    }

    public static void main(String[] args) {
        print();
    }
}
