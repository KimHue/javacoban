import java.util.Scanner;

public class BSCNN {
    private int b;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a,b;

        // dieu kien của a
        do {
            System.out.print("Nhap so a = ");
            a = scan.nextInt();
            if (a < 0) {
                System.out.println("Sai rồi, hãy nhập lại!");
            }
        }while (a<0);

        // dieu kien của b
        do {
            System.out.print("Nhap so b = ");
            b = scan.nextInt();
            if (b < 0) {
                System.out.println("Sai rồi, hãy nhập lại!");
            }
        }while (b<0);

        //bscnn = (a*b)/ucln

//        int ucln;
//        for(ucln=Math.min(a,b);ucln>=1;ucln--)
//        {
//            if(a % ucln == 0 && b % ucln == 0)
//                break;
//        }
//        System.out.println("BCNN = "+(a*b)/ucln);
        System.out.println("BCNN = "+BCLN(a,b));

    }
    public static int BCLN(int a, int b){
        int ucln;
        for(ucln=Math.min(a,b);ucln>=1;ucln--)
        {
            if(a % ucln == 0 && b % ucln == 0)
                break;
        }
        return (a*b)/ucln;
    }
}
