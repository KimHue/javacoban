import java.sql.SQLOutput;
import java.util.Scanner;

public class BT2_XepHangHS {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        float kTra, giuaKi, cuoiKi;

        //Nhập thông tin
        System.out.print("Nhập số điểm kiểm tra: ");
        do{
            kTra = scan.nextFloat();
            if(kTra<0||kTra>10)
                System.out.println("Sai rồi, hãy nhập lại!");
        }while (kTra<0||kTra>10);

        System.out.print("Nhập số điểm kiểm tra giữa kì: ");
        giuaKi = scan.nextFloat();
        System.out.print("Nhập số điểm kiểm tra cuối kì: ");
        cuoiKi = scan.nextFloat();

        float tb = (kTra + giuaKi + cuoiKi) / 3;

        //Xếp loại
        if (tb >= 9)
            System.out.println("hạng A");
        else if (tb >= 7 && tb < 9)
            System.out.println("hạng B");
        else if (tb >= 5 && tb < 7)
            System.out.println("hạng C");
        else
            System.out.println("hạng F");
    }
}
