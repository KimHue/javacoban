package IO;

import java.io.*;

public class CoppyFile {
    static void copyFile(File file1, File file2) {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(file1);
            os = new FileOutputStream(file2);
            // Max length per line = 1024
            byte[] bytes = new byte[1024];

            int length;
            while ((length = is.read(bytes)) > 0) {
                os.write(bytes, 0, length);
            }
            System.out.println("Copy thanh cong");
        } catch (Exception e) {
            System.out.println("Không copy dc file");
        }
    }

    // Doc file
    static String readFile(String filePath) {
        try {
            FileInputStream file = new FileInputStream(filePath);
            String result = "";
            int i;
            while ((i = file.read()) != -1) {
                result += (char) i;
            }
            return result;
        } catch (Exception e) {
            return "Loi chuong Trinh";
        }
    }

    public static void main(String[] args) {
        File oldFile = new File("C:\\Users\\hueph\\Desktop\\HelloWord.txt");

        File newFile = new File("C:\\Users\\hueph\\Desktop\\HelloWordNew.txt");

        copyFile(oldFile, newFile);
        System.out.print("Noi dung file moi la :");
        String path = "C:\\Users\\hueph\\Desktop\\HelloWordNew.txt";
        String text = readFile(path);
        System.out.println(text);

    }
}
