package IO;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ArraySort {
    //Nhap mang
    public static String[] nhapMang() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhap do dai mang n = ");
        int n = scan.nextInt();
        Scanner scan1 = new Scanner(System.in);
        String[] arr = new String[n];
        for (int i = 0; i < n; i++) {
            System.out.print("nhap ptu thu " + i + ": ");
            arr[i] = scan1.nextLine();
        }
        return arr;
    }

    static void sort(String[] array) {

        for (int i = 0; i < array.length-1; i++) {
            //String str[] = array[i].split("");
            if (array[i].compareTo(array[i + 1]) > 0) {
                String temp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = temp;
            }
        }
    }

    static void writeArray(String[] array, String filePath) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePath));
        for (int i = 0; i < array.length; i++) {
            bufferedWriter.write(String.valueOf(array[i]));
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }

    static String readFile(String filePath) {
        try {
            FileInputStream file = new FileInputStream(filePath);
            int i;
            String result = "";
            while ((i = file.read()) != -1) {
                result += (char) i;
            }
            file.close();
            return result;
        } catch (Exception e) {
            return "Loi chuong trinh";
        }
    }

    public static void main(String[] args) {
        String[] array = nhapMang();
        sort(array);
        try {
            String filePath = "C:\\Users\\hueph\\Desktop\\HelloWord.txt";
            writeArray(array, filePath);
            System.out.println("Ghi file thanh cong");
            System.out.println(readFile(filePath));
        } catch (Exception e) {
            System.out.println("Ghi file that bai");
        }

    }
}
