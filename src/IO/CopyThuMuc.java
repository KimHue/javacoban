package IO;

import java.io.*;

public class CopyThuMuc {
    static void copyFile(File file1, File file2) {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(file1);
            os = new FileOutputStream(file2);
            // Sử dụng bytes stream để hỗ trợ việc đọc tất cả các file
            // Max length per line = 1024
            byte[] bytes = new byte[1024];

            int length;
            while ((length = is.read(bytes)) > 0) {
                os.write(bytes, 0, length);
            }
            //copy ok
        } catch (Exception e) {
            System.out.println("Không copy dc file");
        }
    }


    public static void copyFolder(File oldFolder, File newFolder) throws IOException {

        // kiểm oldFolder có tồn tại hay k?
        if (oldFolder.isDirectory()) {

            // kiểm tra newFolder có tồn tại hay không, nếu không tồn tại thì
            // tạo mới
            if (!newFolder.exists()) {
                //mkdir(): tạo ra dường dẫn thư mục duy nhất.
                newFolder.mkdir();
                System.out.println("Thư mục được copy từ " + oldFolder + " đến " + newFolder);
            }

            // lấy danh sách tên các thư mục, file chứa trong thư mục oldFolder
            String files[] = oldFolder.list();

            for (String file : files) {
                // tạo ra các đối tượng thuộc lớp File cho các file và thư mục
                // từ danh sách đã get ở trên
                File srcFile = new File(oldFolder, file);
                File destFile = new File(newFolder, file);
                // thực hiện đệ quy (gọi lại phương thức copyFolder)
                copyFolder(srcFile, destFile);
            }

        } else {
            // nếu là file thì thực hiện copy

            // copy file ở đây là đọc nội dung file
            // sau đó ghi nội dung file đó vào file ở thư mục newFolder
            copyFile(oldFolder, newFolder);
        }
    }

    public static void main(String[] args) {
        File oldFolder = new File("C:\\Users\\hueph\\Desktop\\Old_folder");

        File newFolder = new File("C:\\Users\\hueph\\Desktop\\New_folder");
        // kiểm tra thư mục muốn copy đã tồn tại hay chưa
        if (!oldFolder.exists()) {

            System.out.println("Thư mục chưa tồn tại.");
            // thoát chương trình
            System.exit(0);
        } else {
            try {
                // sử dụng phương thức copyFolder được viết bên dưới
                copyFolder(oldFolder, newFolder);
            } catch (IOException e) {
                e.printStackTrace();
                // có lỗi, thoát chương trình ngay
                System.exit(0);
            }
        }

        System.out.println("Copy thư mục thành công");
    }
}
