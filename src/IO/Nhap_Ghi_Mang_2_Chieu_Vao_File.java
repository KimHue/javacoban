package IO;

import java.io.*;
import java.util.Scanner;

public class Nhap_Ghi_Mang_2_Chieu_Vao_File {
    public static String[][] inputArr() {
        Scanner scan = new Scanner(System.in);
        System.out.println("n = ");
        int n = scan.nextInt();
        System.out.println("m = ");
        int m = scan.nextInt();
        String[][] arr = new String[n][m];
        Scanner scan1 = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print("nhap phan tu thu [" + i + "],[" + j + "]: ");
                arr[i][j] = scan1.nextLine();
            }
        }
        return arr;
    }

    //Ghi file
    static void writeArray(String[][] array, String filePath) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePath));
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                bufferedWriter.write(String.valueOf(array[i][j]));
            }
            bufferedWriter.newLine();

        }

        bufferedWriter.close();
    }

    static String readFile(String filePath) {
        try {
            FileInputStream file = new FileInputStream(filePath);
            int i;
            String result = "";
            while ((i = file.read()) != -1) {
                result += (char) i;
            }
            file.close();
            return result;
        } catch (Exception e) {
            return "Loi chuong trinh";
        }
    }

    public static void main(String[] args) {
        String[][] array = inputArr();

        try {
            String filePath = "C:\\Users\\hueph\\Desktop\\HelloWord.txt";
            writeArray(array, filePath);
            System.out.println("Ghi file thanh cong");
            System.out.println(readFile(filePath));
        } catch (Exception e) {
            System.out.println("Ghi file that bai");
        }
    }
}
