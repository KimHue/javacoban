import java.util.Scanner;

public class StringPratice {
    public static void main(String[] args){
//        String s1="Hello";
//        String s2="World";
//        String s3=s1+s2;
//        String s4=s1.concat(s2);
//        System.out.println(s3);
//        System.out.println(s4);
//
//        int length = s1.length();
//        System.out.println(length);
//        System.out.println(s1.charAt(1));
//        System.out.println(s2.compareTo(s1));
//        String s5="Pham Thi Kim Hue";
//        String s6 ="Hue";
//        System.out.println(s5.indexOf(s6));
//        System.out.println(s5.lastIndexOf('h'));
//
//        StringBuilder sb =new StringBuilder();
//        sb.append("wellcome to");
//        sb.append("\tJava");
//        System.out.println(sb);

        final float pi = 3.14f;
        Scanner sc = new Scanner(System.in);
        float r = sc.nextFloat();
        System.out.format("Circumference = %.1f",(r*2*pi));
    }
}
