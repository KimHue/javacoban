package Example;

import java.util.Scanner;

public class Try_Catch {

    static int inpuNumber() throws Exception{
        Scanner scan = new Scanner(System.in);
        System.out.println("Nhap n = ");
        int a = scan.nextInt();
        System.out.println(a);
        return a;
    }

    static int inpuNumber1() throws Exception{

        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Nhap n1 = ");
            int a = scan.nextInt();
            System.out.println(a);
            return a;
        }catch (Exception e){
            throw new Exception("Ban da nhap sai:");
        }
    }

    public static void main(String[] args) {
//        System.out.println("Nhap n = ");
//        do {
//            try {
//                Scanner scan = new Scanner(System.in);
//                int a = scan.nextInt();
//                break;
//            } catch (Exception e) {
//                System.out.println("Sai r");
//            } finally {
//                System.out.println("Finish");
//            }
//        } while (true);

        try {
            int a = inpuNumber();
        }catch (Exception e){
            System.out.println("Sai r");
        }

        try {
            int b = inpuNumber1();
        }catch (Exception e){
           System.out.println(e.getMessage());
        }
    }
}
