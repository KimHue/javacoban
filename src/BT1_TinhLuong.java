import java.util.Scanner;

public class BT1_TinhLuong {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Nhập lương nhân viên: ");

        double salary = scan.nextDouble();
        double tax, netSalary;
        if(salary<0)
        {
            System.out.println("Lương nhân viên phải là số dương, vui lòng nhập lại!");
        }
        else
        {
            if (salary >= 15)
                tax = salary * 30 / 100;
            else if (salary >= 7)
                tax = salary * 20 / 100;
            else
                tax = salary * 10 / 100;

            netSalary = salary - tax;
            System.out.println("Lương thực sự: " + netSalary + " và thuế: " + tax);
        }
    }
}
