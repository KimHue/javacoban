import java.util.Scanner;

public class BT3_GiaiPTBacHai {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        float a, b, c;
        System.out.println("--------Giải Phương Trình Bậc Hai-------");
        System.out.print("a = ");
        a = scan.nextFloat();
        System.out.print("b = ");
        b = scan.nextFloat();
        System.out.print("c = ");
        c = scan.nextFloat();

        // delta && canDelta
        float delta = (b*b) - (4 * a * c);
        double canDelta = Math.sqrt(delta);

        // Giải pt với các điều kiện
        if (a == 0 && b!=0)
            System.out.println("Pt có một nghiệm duy nhất x = " + (-c / b));
        else if (a == 0 && b == 0)
            System.out.println("Pt vô nghiệm");
        else if (delta < 0)
            System.out.println("Pt vô nghiệm");
        else if (delta == 0)
            System.out.println("Pt có nghiệm kép: " + (-b / (2 * a)));
        else
            System.out.println("Pt có hai nghiệm pb: x1 = " + ((-b + canDelta) / (2 * a)) + "\tx2 = " + ((-b - canDelta) / (2 * a)));
    }
}
