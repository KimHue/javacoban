import java.util.Locale;
import java.util.Scanner;

public class Bt_String {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhập s1: ");
        String s1=scan.nextLine();
        System.out.print("Nhập s2: ");
        String s2=scan.nextLine();
        String s3=s1.concat(s2);

        // Cộng chuỗi
        System.out.println(s3);
        //IN HOA
        System.out.println(s3.toUpperCase());
        //Đảo ngược
        StringBuilder str = new StringBuilder(s3);
        System.out.println(str.reverse());
        //So sánh hai chuỗi
        System.out.println(s1.compareTo(s2));   
    }
}
