package Array;

import java.util.Scanner;

public class BT1_Array {
    public static int[] nhapMang() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Nhap do dai mang n = ");
        int n = scan.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("nhap ptu thu " + i + ": ");
            arr[i] = scan.nextInt();
        }
        return arr;
    }

    public static int timGTLN(int[] a) {
        int max = a[0];
        for (int i = 1; i < a.length; i++) {
            if (max < a[i]) {
                max = a[i];
            }
        }
        return max;
    }

    public static int timGTNN(int[] a) {
        int min = a[0];
        for (int i = 1; i < a.length; i++) {
            if (min > a[i])
                min = a[i];
        }
        return min;
    }

    public static int tinhTong(int[] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }

    public static boolean kiemTraDX(int a[]) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] != a[a.length - i - 1]) {
                return false;
            }
        }
        return true;
    }

    //Sap xep mang
    public static void sapXep(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 1; j < a.length; j++) {
                int temp = a[i];
                if (a[i] > a[j]) {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
    }

    public static void xuatMang(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }

    public static void main(String[] args) {
        int[] arr = nhapMang();
        System.out.println("Gia tri max: " + timGTLN(arr));
        System.out.println("Gia tri min: " + timGTNN(arr));
        if (kiemTraDX(arr))
            System.out.println("la mang dx");
        else
            System.out.println("k phai mang doi xung");

        sapXep(arr);
        xuatMang(arr);
    }
}
