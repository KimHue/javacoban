package Array;

import java.util.Scanner;

public class BT_ChenPtu {
    public static Scanner scan = new Scanner(System.in);

    public static int nhapSo() {
        int n;
        do {
            System.out.print("Nhập vào số phần tử của mảng: ");
            n = scan.nextInt();
        } while (n < 0);
        return n;
    }

    public static int nhapIndex(int[] a) {
        int k;
        do {
            System.out.print("Nhập vtri can thay: ");

            k = scan.nextInt();
            if (k < 0 || k >= a.length) {
                System.out.println("Sai roi , hay nhap lai");
            }
        } while (k < 0 || k >= a.length);
        return k;
    }

    public static void chenPhanTu(int[] a, int k, int x) {
        int n = a.length;
        int[] newArr = new int[n + 1];

        int index = 0;
        for (int i = 0; i < a.length; i++) {
            if (i == k)
                newArr[index++] = x;
            newArr[index] = a[i];
            index++;
        }


        for (int pt : newArr) {
            System.out.print(pt + " ");
        }
    }

    public static void nhapMang(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print("Nhập phần tử thứ a[" + i + "]: ");
            a[i] = scan.nextInt();
        }
    }

    public static void main(String[] args) {
        int n = nhapSo();
        int[] a = new int[n];
        nhapMang(a);
        int k = nhapIndex(a);
        System.out.print("Nhap gia tri: ");
        int x = scan.nextInt();
        chenPhanTu(a, k, x);
    }
}
