package Array;

import java.util.Scanner;

public class ArrayDimension {
    public static int[] inputArr() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Nhap do dai mang n = ");
        int n = scan.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("nhap ptu thu a" + i);
            arr[i] = scan.nextInt();
        }
        return arr;
    }
    public static void main(String[] args) {
        int[] arr1 = inputArr();
        int[] a = {1, 2, 3, 4, 5, 6};
        //khoi tao mang co 4 ptu
        int[] arr=new int[4];
        arr[0] = 5;
        arr[1] = 6;
        arr[2] = 3;
        arr[3] = 4;
        System.out.println(a[0]);
        System.out.println("do dai cua mang la :" + a.length);

        for(int i =0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
        // tao mang string
        String[] arrString = {"a","b","c"};
        for(int i =0;i<arrString.length;i++){
            System.out.print(arrString[i]+" ");
        }
    }
}
