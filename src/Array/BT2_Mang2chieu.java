package Array;

import java.util.Scanner;

public class BT2_Mang2chieu {
    public static int[][] inputArr() {
        Scanner scan = new Scanner(System.in);
        System.out.println("n = ");
        int n = scan.nextInt();
        System.out.println("m = ");
        int m = scan.nextInt();
        int[][] arr = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print("nhap phan tu thu " + i + "," + j + ": ");
                arr[i][j] = scan.nextInt();
            }
        }
        return arr;
    }

    public static int timMax(int[][] a) {
        int max = a[0][0];
        for (int i = 0; i < a.length; i++) {
            for (int j = 1; j < a[i].length; j++) {
                if (max < a[i][j]) {
                    max = a[i][j];
                }
            }
        }
        return max;
    }

    public static int timMin(int[][] a) {
        int min = a[0][0];
        for (int i = 0; i < a.length; i++) {
            for (int j = 1; j < a[i].length; j++) {
                if (min > a[i][j]) {
                    min = a[i][j];
                }
            }
        }
        return min;
    }

    public static int tinhTong(int[][] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                sum += a[i][j];
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        int arr[][] = inputArr();
        System.out.println("gia tri max: " + timMax(arr));
        System.out.println("gia tri min : " + timMin(arr));
        System.out.println("Tong cua mang:  " + tinhTong(arr));

    }
}
