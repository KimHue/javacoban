package Array;

import java.util.Scanner;

public class Array2Dimension {
    public static int[][] inputArr() {
        Scanner scan = new Scanner(System.in);
        System.out.println("n = ");
        int n = scan.nextInt();
        System.out.println("m = ");
        int m = scan.nextInt();
        int[][] arr = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print("nhap phan tu thu "+i+","+j+": ");
                arr[i][j]=scan.nextInt();
            }
        }
        return arr;
    }
    public static void main(String[] args) {
        //khai bao mang 2 chieu
        int[][] array = {
                {1, 2}, {3, 4}, {5, 6}
        };

        int[][] a = inputArr();
        System.out.print(array.length);
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
        }

        System.out.println();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
        }

    }
}
