package Array;

import java.util.Scanner;

public class BTTongHop {
    public static Scanner scan = new Scanner(System.in);

    public static void inMenu() {
        System.out.print("1. Tính trung bình cộng các số lẻ ở vị trí chẵn\n" +
                "2. Hiện thị các số nguyên tố có trong mảng lên màn hình\n" +
                "3. Tạo 1 mảng mới với các giá trị đảo ngược so với mảng cũ\n" +
                "4. TÌm phẩn tử lớn thứ 2 trong mảng\n" +
                "5. Chia đôi 1 mảng thành 2 mảng\n" +
                "6. Gộp 2 mảng thành 1 mảng\n" +
                "*************************************************************\n");
    }

    public static int nhapSo() {
        int n;
        do {
            System.out.print("Nhập vào số phần tử của mảng: ");
            n = scan.nextInt();
        } while (n < 0);
        return n;
    }


    public static void nhapMang(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print("Nhập phần tử thứ a[" + i + "]: ");
            a[i] = scan.nextInt();
        }
    }

    public static void xuatMang(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }

    //Tính trung bình cộng các số lẻ ở vị trí chẵn
    public static float tinhTBCSoLeViTriChan(int[] a) {
        int sum = 0;
        int dem = 0;
        for (int i = 0; i < a.length; i++) {
            if (i % 2 == 0 && a[i] % 2 != 0) {
                sum += a[i];
                dem++;
            }
        }
        float tbc = sum / dem;
        return tbc;
    }

    //Ham la so nguyen to
    public static boolean laSoNguyenTo(int n) {
        if (n < 2) {
            return false;
        }
        for (int i = 2; i < n; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    //Tạo 1 mảng mới với các giá trị đảo ngược so với mảng cũ
    public static void inMangDaoNguoc(int[] a) {
        for (int i = 0; i < a.length / 2; i++) {
            int temp = a[i];
            a[i] = a[a.length - i - 1];
            a[a.length - i - 1] = temp;
        }
        xuatMang(a);
    }

    //Hiện thị các số nguyên tố có trong mảng lên màn hình
    public static void inMangNguyenTo(int[] a) {
        for (int i = 0; i < a.length; i++) {
            if (laSoNguyenTo(a[i])) {
                System.out.print(a[i] + " ");
            }
        }
    }

    //Tach thanh hai mang
    public static void tachMang(int[] a, int n) {
        int[] arr1 = new int[n];
        int[] arr2 = new int[n];
        int arr1Length = 0, arr2Length = 0;
        // duyệt vòng lặp for từ đầu đến giữa mảng và dồn vào arr1
        for (int i = 0; i < (n / 2); i++) {
            arr1[arr1Length] = a[i];
            arr1Length++;
        }

        for (int i = (n / 2); i < n; i++) {
            arr2[arr2Length] = a[i];
            arr2Length++;
        }
        System.out.print("Mảng thứ nhất: ");
        for (int i = 0; i < arr1Length; i++) {
            System.out.print(arr1[i] + " ");
        }
        System.out.print("Mảng thứ hai: ");
        for (int i = 0; i < arr2Length; i++) {
            System.out.print(arr2[i] + " ");
        }
    }

    //Phần tử lớn thứ 2 trong mảng
    public static int timPhanTuMax2(int[] a) {
        int max = a[0];
        for (int i = 1; i < a.length; i++) {
            if (max < a[i]) {
                max = a[i];
            }
        }
        int max2 = a[0];
        for (int i = 0; i < a.length; i++) {
            if (a[i] > max2 && a[i] < max) {
                max2 = a[i];
            }
        }
        return max2;
    }

    //Trộn hai mảng
    public static void tronMang(int[] a, int[] b) {
        int n = a.length + b.length;
        //Tao mang moi
        int[] newArr = new int[n];
        int ptu = 0;

        //duyet mang a
        for (int i = 0; i < a.length; i++) {
            //don mang a vào mảng newArr
            newArr[ptu] = a[i];
            ptu++;
        }
        //duyet mang b
        for (int i = 0; i < b.length; i++) {
            //don mang b vào mảng newArr
            newArr[ptu] = b[i];
            ptu++;
        }
        xuatMang(newArr);
    }

    //Ham xu li
    public static void xuLi(int[] a, int n) {
        int chon;
        do {
            System.out.print("\nHãy chọn Menu: ");
            chon = scan.nextInt();
            if (chon < 1 || chon > 6) {
                System.out.println("Làm ơn nhập từ 1 - 6!!!");
            }
        } while (chon < 1 || chon > 6);
        switch (chon) {
            case 1:
                System.out.print("Tổng trung bình cộng các số lẻ ở vtri chẵn là: " + tinhTBCSoLeViTriChan(a));
                break;
            case 2:
                System.out.print("Mảng nguyên tố: ");
                inMangNguyenTo(a);
                break;
            case 3:
                System.out.print("Mảng đảo ngược: ");
                inMangDaoNguoc(a);
                break;
            case 4:
                System.out.println("Phần tử lớn thứ 2 trong mảng: " + timPhanTuMax2(a));
                break;
            case 5:
                tachMang(a, n);
                break;
            case 6:
                int m = nhapSo();
                int[] b = new int[m];
                System.out.println("Hãy nhập mảng thứ hai:  ");
                nhapMang(b);
                tronMang(a, b);
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {
        inMenu();
        int n = nhapSo();
        int[] a = new int[n];
        nhapMang(a);
        System.out.print("mảng vừa nhâp: ");
        xuatMang(a);
        xuLi(a, n);
    }
}
