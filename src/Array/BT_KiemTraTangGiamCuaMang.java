package Array;

import java.util.Scanner;

public class BT_KiemTraTangGiamCuaMang {
    public static Scanner scan = new Scanner(System.in);

    public static int nhapSo() {
        int n;
        do {
            System.out.print("Nhập vào số phần tử của mảng: ");
            n = scan.nextInt();
        } while (n < 0);
        return n;
    }

    public static void inputIntArr(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print("Nhập phần tử thứ a[" + i + "]: ");
            a[i] = scan.nextInt();
        }
    }

    public static void inputStringArr(String[] a) {
       // Scanner scan = new Scanner(System.in);
        for (int i = 0; i < a.length; i++) {
            System.out.print("Nhập phần tử thứ a[" + i + "]: ");
            a[i] = scan.nextLine();

        }
    }

    public static boolean isASC_String(String[] a) {
        int flag = 0;
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i].compareTo(a[i + 1]) > 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isDESC_String(String[] a) {
        int flag = 0;
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i].compareTo(a[i + 1]) < 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isASC_ArrInt(int a[]) {
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] > a[i + 1])
                return false;
        }
        return true;
    }

    public static boolean isDESC_ArrInt(int a[]) {
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] < a[i + 1])
                return false;
        }
        return true;
    }

    public static void Action(int[] a, String[] arrStr) {
        if (isASC_ArrInt(a))
            System.out.println("Mang tang");
        else if (isDESC_ArrInt(a))
            System.out.println("Mang giam");
        else
            System.out.println("Mang chua sap xep");

        if(isASC_String(arrStr))
            System.out.println("Mang tang");
        else if(isDESC_String(arrStr))
            System.out.println("Mang giam");
        else
            System.out.println("Mang chua sap xep");
    }

    public static void main(String[] args) {
        //int n = nhapSo();
       // int[] a = new int[n];
       // inputIntArr(a);

//        int m = nhapSo();
        String[] stringArr = new String[2];
        inputStringArr(stringArr);
//        int n = nhapSo();
        String[] stringArr1 = new String[3];

        inputStringArr(stringArr1);
       // Action(a,stringArr);
    }
}
