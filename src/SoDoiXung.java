import java.util.Scanner;

public class SoDoiXung {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int number,reversed=0;
        do{
            System.out.print("Nhập number = ");
            number = scan.nextInt();
        }while (number<0);

//        // kiem tra so doi xung
//        int n = number;
//        while (number!=0){
//            int temp = number%10;
//            reversed=reversed*10+temp;
//            number/=10;
//        }
//        String str = (n==reversed)?"Là số đối xứng":"Không phải số đối xứng";
//        System.out.println(str);

        if(laSoDoiXung(number))
            System.out.println("la so dx");
        else
            System.out.println("k phai so dx");
    }
    public static boolean laSoDoiXung(int number){
        int n = number;
        int reversed = 0;
        while (number!=0){
            int temp = number%10;
            reversed=reversed*10+temp;
            number/=10;
        }
        if(n==reversed)
            return true;
        else
            return false;
    }

}
