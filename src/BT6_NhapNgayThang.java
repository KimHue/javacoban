import java.util.Scanner;

public class BT6_NhapNgayThang {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
       int ngay,thang,nam;

       do{
           ngay = scan.nextInt();
           if(ngay<0||ngay>31)
           {
               System.out.println("không tồn tại ngày này, vui lòng nhập lai!");
           }

       }while (ngay<0||ngay>31);

       do{
           thang = scan.nextInt();
           if(thang<0||thang>12)
           {
               System.out.println("không tồn tại tháng này, vui lòng nhập lai!");
           }

       }while (thang<0||thang>12);

       do{
           nam = scan.nextInt();
           if(nam<0)
           {
               System.out.println("không tồn tại năm này, vui lòng nhập lai!");
           }
       }while (nam<0);

       //So ngay trong thang
        int soNgay=31;
        switch (thang) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                soNgay = 31;
                break;
            case 2:
            case 4:
            case 6:
            case 9:
            case 11:
                soNgay = 30;
                break;
        }

        if(ngay==soNgay)
        {
            if(thang==12)
            {
                ngay=1;
                thang=1;
                nam++;
            }
            else
            {
                ngay=1;
                thang++;
            }

        }
        else
            ngay++;
        System.out.println("Ngay tiep theo la: "+ngay+"/"+thang+"/"+nam);
    }
}
