import java.util.Scanner;

public class BT4_TinhCuocDT {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        final int phiThueBao = 25;
        System.out.print("Hãy nhập số phút bạn đã sử dụng: ");
        float soPhut = scan.nextFloat();
        float phiPhaiTra = 0, tongTien;

        if (soPhut > 200)
            phiPhaiTra = (soPhut - 200) * 200 + 150 * 400 + 50 * 600;
        else if (soPhut > 50)
            phiPhaiTra = (soPhut - 50) * 400 + 50 * 600;
        else
            phiPhaiTra = soPhut * 600;

        tongTien = phiPhaiTra + phiThueBao;
        System.out.println("Ban da goi " + soPhut + " và tổng tiền là : " + tongTien);

    }
}
