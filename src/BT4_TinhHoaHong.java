import java.util.Scanner;

public class BT4_TinhHoaHong {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhập doanh số: ");
        float doanhSo = scan.nextFloat();
        float tienHoaHong;
        if (doanhSo <= 100) {
            tienHoaHong = doanhSo * 5 / 100;
        } else if (doanhSo <= 300) {
            tienHoaHong = doanhSo * 10 / 100;
        } else {
            tienHoaHong = doanhSo * 20 / 100;
        }
        System.out.println("Doanh số " + doanhSo + " bạn se nhận được " + tienHoaHong + " tiền hoa hồng");
    }
}
