import java.util.Scanner;

public class TinhGiaiThua {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n,giaiThua=1;
        do{
            System.out.print("Nhập n = ");
            n=scan.nextInt();
        }while (n<0);

        System.out.println(n+"! = "+tinhGiaiThua(n));
    }
    public static int tinhGiaiThua(int n){
        int giaiThua = 1;
        for(int i = 1;i<=n;i++)
        {
            giaiThua*=i;
        }
        return giaiThua;
    }
}
