import java.util.Scanner;

public class BT7_Tong4So {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int number ;
        do{
            System.out.print("Nhap number = ");
            number=scan.nextInt();
            if(number<1000||number>9999)
            {
                System.out.println("Nhap lai ");
            }
        }while (number<1000||number>9999);

        int sum=0, temp;
        while (number!=0)
        {
            temp=number%10;
            sum+=temp;
            number/=10;
        }
        System.out.println(sum);
    }
}
